## Livestream Test-Home Task

Test coding challenge on iOS Developer position.

Current architecture implemented as MVVM with additional Presenter layer.
Implemented in pure ObjC.

### To start

You need [CocoaPods](https://guides.cocoapods.org/using/getting-started.html).
```
$ pod install
$ open Livestream\ Test-Home\ Task.xcworkspace
```
* Run
*just run…*

### TODO

```
$ find . -name \*.m | xargs grep TODO
./Comment.m:    // TODO: implement me!
./Like.m:    // TODO: implement me!
./Status.m:    // TODO: reduce nested path parts duplication
./Statuses.m:    // TODO: move viewModeller, DataSource and data logic to StatusesPresenter.
./Statuses.m:    // TODO: cleanup models
./Statuses.m:         // TODO: improve animation
```

* organize architecture layers by folders
* think about pagination
* improve network failure tolerance
