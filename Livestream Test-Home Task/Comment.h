//
//  Comment.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface Comment : MTLModel <MTLJSONSerializing>

// TODO: implement according to example below

/*
 "id": 9399980,
 "author_id": 113,
 "author": {
 "id": 113,
 "full_name": "Mobi Dev",
 "short_name": "mobiledev",
 "picture": {
 "original_width": 960,
 "original_height": 960,
 "url": "http://img.new.livestream.com/accounts/0000000000000071/7d8dea1f-3d4a-45b7-bd4c-da9a627cf6b9.jpg",
 "thumb_url": "http://img.new.livestream.com/accounts/0000000000000071/7d8dea1f-3d4a-45b7-bd4c-da9a627cf6b9_50x50.jpg",
 "small_url": "http://img.new.livestream.com/accounts/0000000000000071/7d8dea1f-3d4a-45b7-bd4c-da9a627cf6b9_170x170.jpg",
 "medium_url": null,
 "secure_thumb_url": "https://img.new.livestream.com/accounts/0000000000000071/7d8dea1f-3d4a-45b7-bd4c-da9a627cf6b9_50x50.jpg",
 "secure_small_url": "https://img.new.livestream.com/accounts/0000000000000071/7d8dea1f-3d4a-45b7-bd4c-da9a627cf6b9_170x170.jpg",
 "secure_medium_url": null,
 "width": 960,
 "height": 960
 },
 "is_beta_producer": true,
 "timezone": "Europe/Zaporozhye",
 "description": "Check out <a href=\"http://www.mywebsite.com\">my website</a> for more info.",
 "background_image": null,
 "background_color": "#EFEFF7",
 "background_repeat": "no-repeat",
 "background_position": "center top",
 "background_attachment": null,
 "created_at": "2011-10-25T08:39:15.000Z",
 "is_locked": false,
 "upcoming_events": null,
 "past_events": null,
 "links": [
 {
 "title": "linke1",
 "url": "http://new.livestream.com"
 },
 {
 "title": "link2",
 "url": "http://new.livestream.com"
 },
 {
 "title": "linke1",
 "url": "http://new.livestream.com"
 }
 ],
 "google_analytics_id": null,
 "mixpanel_id": "a38174f33184c-0014ee8a8-43681f0a-2ee000-a38174f332d50",
 "signup_page": null,
 "signup_action": null,
 "devices": {
 "total": 0,
 "data": []
 },
 "category": 8,
 "category_name": null,
 "private": false,
 "features": "512",
 "plan_id": 9,
 "plan_info": {
 "id": 9,
 "features": "67102846",
 "type": 40,
 "is_scoped": false
 },
 "features_with_plan": "67103358",
 "is_searchable": true,
 "is_public": true,
 "ad_account_id": "6878/dev4on3",
 "ad_provider_id": "dfp",
 "ad_custom_params": {
 "accountId": "%accountId%",
 "eventId": "%eventId%",
 "live": "%isLive%",
 "embedPath": "%embedPath%",
 "embedHost": "%embedHost%"
 },
 "ad_enabled_for_vod": true,
 "ad_enabled_for_live": true,
 "ad_enabled_for_owner": false,
 "ad_types": [
 "preroll"
 ],
 "ad_enabled": true,
 "followers": null,
 "following": null
 },
 "post_key": "status:507049",
 "text": "at last",
 "created_at": "2013-01-15T07:42:37.924Z",
 "author_type": null,
 "author_full_name": null
*/

@end
