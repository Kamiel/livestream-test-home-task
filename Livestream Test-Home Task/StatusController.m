//
//  StatusController.m
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "StatusController.h"
#import "Status.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import <NSDate_TimeAgo/NSDate+TimeAgo.h>


@interface StatusController ()

@property (nonatomic, strong, readwrite, nonnull) Status *model;

@property (nonatomic, strong, readwrite, nonnull) RACSignal<NSString *> *text;
@property (nonatomic, strong, readwrite, nonnull) RACSignal<NSString *> *date;
@property (nonatomic, strong, readwrite, nonnull) RACSignal<NSString *> *views;

@end


@implementation StatusController

- (instancetype _Nullable)init
{
    if (self = [super init]) {
        [self bind];
    }
    return self;
}

- (instancetype _Nullable)initWithModel:(Status *_Nonnull)status
{
    if (self = [self init]) {
        self.model = status;
    }
    return self;
}

- (void)bind
{
    self.text = [RACObserve(self, model.text) map:
                 ^NSString *_Nonnull(NSString *_Nullable string) {

                     return
                     string ?: NSLocalizedString(@"Nothing to see here...",
                                                 @"No status text available.");
                 }];
    self.date = [RACObserve(self, model.updated) map:
                 ^NSString *_Nonnull(NSDate *_Nullable date) {

                     return date ? [date timeAgo]
                     : NSLocalizedString(@"Once", @"No status date available.");
                 }];
    self.views = [RACObserve(self, model.views) map:
                  ^NSString *_Nonnull(NSNumber *_Nullable number) {

                      return
                      [NSString stringWithFormat:
                       NSLocalizedString(@"%tu views",
                                         @"{count} of views."),
                       number != nil ? number.unsignedIntegerValue : 0u];
                  }];
}

@end
