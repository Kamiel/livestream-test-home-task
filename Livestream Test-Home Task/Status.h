//
//  Status.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <Mantle/Mantle.h>


@class Like, Comment;


@interface Status : MTLModel <MTLJSONSerializing>

@property (nonatomic, assign, readonly)           NSUInteger          identifier;
@property (nonatomic, assign, readonly)           NSUInteger          eventID;
@property (nonatomic, copy, readonly, nullable)   NSString           *text;

// ???: there's no information about body & tags data format,
//      they're usually null.

//@property body;
//@property tags;

@property (nonatomic, assign, readonly)           BOOL                draft;
@property (nonatomic, strong, readonly, nullable) NSDate             *created;
@property (nonatomic, strong, readonly, nullable) NSDate             *updated;
@property (nonatomic, strong, readonly, nullable) NSDate             *publish;
@property (nonatomic, assign, readonly)           NSUInteger          views;
@property (nonatomic, copy, readonly, nonnull)    NSArray<Like *>    *likes;
@property (nonatomic, copy, readonly, nonnull)    NSArray<Comment *> *comments;

@end
