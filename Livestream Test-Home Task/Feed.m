//
//  Feed.m
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "Feed.h"
#import "Status.h"
#import "MTLJSONAdapter+ArrayConditionalTransformer.h"


@implementation Feed

#pragma mark - life cycle

- (instancetype _Nullable)init
{
    if (self = [super init]) {
        self->_data = @[]; // because nonnull
    }
    return self;
}

#pragma mark - MTLJSONSerializing

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return
    @{ @"data" : @"feed.data" };
}

+ (NSValueTransformer *)dataJSONTransformer
{
    return
    [MTLJSONAdapter arrayTransformerWithModelClass:
     [Status class]
     byCondition:
     ^BOOL(NSDictionary<NSString *,
           NSString *> *_Nonnull element) {

         return
         [element[@"type"] isEqualToString:
          @"status"];
     }];
}

@end
