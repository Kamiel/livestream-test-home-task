//
//  StatusCell.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <UIKit/UIKit.h>


@class StatusPresenter;


@interface StatusCell : UICollectionViewCell

@property (weak, nonatomic, nullable) IBOutlet UIView *background;
@property (strong, nonatomic, readonly, nonnull) StatusPresenter *presenter;

@end
