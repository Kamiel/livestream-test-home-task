//
//  Status.m
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "Status.h"
#import "Constants.h"

#import "Like.h"
#import "Comment.h"


@implementation Status

#pragma mark - life cycle

- (instancetype _Nullable)init
{
    if (self = [super init]) {
        self->_likes = @[]; // because nonnull
        self->_comments = @[]; // because nonnull
    }
    return self;
}

#pragma mark - MTLJSONSerializing

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    // TODO: reduce nested path parts duplication
    return
    @{ @"identifier" : @"data.id",
       @"eventID"    : @"data.event_id",
       @"text"       : @"data.text",
       @"draft"      : @"data.draft",
       @"created"    : @"data.created_at",
       @"updated"    : @"data.updated_at",
       @"publish"    : @"data.publish_at",
       @"views"      : @"data.views",
       @"likes"      : @"data.likes.data",
       @"comments"   : @"data.comments.data" };
}

+ (NSValueTransformer *)JSONTransformerForKey:(NSString *)key
{
    if ([key isEqualToString:@"created"] ||
        [key isEqualToString:@"updated"] ||
        [key isEqualToString:@"publish"]) {
        
        return
        [NSValueTransformer mtl_dateTransformerWithDateFormat:kDateFormat
                                                       locale:
         [NSLocale localeWithLocaleIdentifier:kDateLocale]];
    }
    return nil;
}

+ (NSValueTransformer *)likesJSONTransformer
{
    return
    [MTLJSONAdapter arrayTransformerWithModelClass:
     [Like class]];
}

+ (NSValueTransformer *)commentsJSONTransformer
{
    return
    [MTLJSONAdapter arrayTransformerWithModelClass:
     [Comment class]];
}

@end
