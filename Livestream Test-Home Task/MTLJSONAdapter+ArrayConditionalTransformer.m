//
//  MTLJSONAdapter+ArrayConditionalTransformer.m
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "MTLJSONAdapter+ArrayConditionalTransformer.h"


@implementation MTLJSONAdapter (ArrayConditionalTransformer)

/// Sadly, there's no way to DRY here because of lack inheritance reusability.
/// Hint: maybe, more clean way is to override OVCResponse here.
+ (NSValueTransformer<MTLTransformerErrorHandling> *)arrayTransformerWithModelClass:(Class)modelClass
                                                                        byCondition:(BOOL(^)(NSDictionary<NSString *,
                                                                                             NSString *> *_Nonnull element))condition
{
    id<MTLTransformerErrorHandling> dictionaryTransformer = [self dictionaryTransformerWithModelClass:modelClass];
    
    return [MTLValueTransformer
            transformerUsingForwardBlock:^ id (NSArray *dictionaries, BOOL *success, NSError **error) {
                if (dictionaries == nil) return nil;
                
                if (![dictionaries isKindOfClass:NSArray.class]) {
                    if (error != NULL) {
                        NSDictionary *userInfo = @{
                                                   NSLocalizedDescriptionKey: NSLocalizedString(@"Could not convert JSON array to model array", @""),
                                                   NSLocalizedFailureReasonErrorKey: [NSString stringWithFormat:NSLocalizedString(@"Expected an NSArray, got: %@.", @""), dictionaries],
                                                   MTLTransformerErrorHandlingInputValueErrorKey : dictionaries
                                                   };
                        
                        *error = [NSError errorWithDomain:MTLTransformerErrorHandlingErrorDomain code:MTLTransformerErrorHandlingErrorInvalidInput userInfo:userInfo];
                    }
                    *success = NO;
                    return nil;
                }
                
                NSMutableArray *models = [NSMutableArray arrayWithCapacity:dictionaries.count];
                for (id JSONDictionary in dictionaries) {
                    if (JSONDictionary == NSNull.null) {
                        [models addObject:NSNull.null];
                        continue;
                    }
                    
                    if (![JSONDictionary isKindOfClass:NSDictionary.class]) {
                        if (error != NULL) {
                            NSDictionary *userInfo = @{
                                                       NSLocalizedDescriptionKey: NSLocalizedString(@"Could not convert JSON array to model array", @""),
                                                       NSLocalizedFailureReasonErrorKey: [NSString stringWithFormat:NSLocalizedString(@"Expected an NSDictionary or an NSNull, got: %@.", @""), JSONDictionary],
                                                       MTLTransformerErrorHandlingInputValueErrorKey : JSONDictionary
                                                       };
                            
                            *error = [NSError errorWithDomain:MTLTransformerErrorHandlingErrorDomain code:MTLTransformerErrorHandlingErrorInvalidInput userInfo:userInfo];
                        }
                        *success = NO;
                        return nil;
                    }
                    
                    // !!!: additional logic here >>>
                    if (! condition((NSDictionary *)JSONDictionary)) {
                        continue;
                    }
                    // !!!: additional logic here <<<
                    
                    id model = [dictionaryTransformer transformedValue:JSONDictionary success:success error:error];
                    
                    if (*success == NO) return nil;
                    
                    if (model == nil) continue;
                    
                    [models addObject:model];
                }
                
                return models;
            }
            reverseBlock:^ id (NSArray *models, BOOL *success, NSError **error) {
                if (models == nil) return nil;
                
                if (![models isKindOfClass:NSArray.class]) {
                    if (error != NULL) {
                        NSDictionary *userInfo = @{
                                                   NSLocalizedDescriptionKey: NSLocalizedString(@"Could not convert model array to JSON array", @""),
                                                   NSLocalizedFailureReasonErrorKey: [NSString stringWithFormat:NSLocalizedString(@"Expected an NSArray, got: %@.", @""), models],
                                                   MTLTransformerErrorHandlingInputValueErrorKey : models
                                                   };
                        
                        *error = [NSError errorWithDomain:MTLTransformerErrorHandlingErrorDomain code:MTLTransformerErrorHandlingErrorInvalidInput userInfo:userInfo];
                    }
                    *success = NO;
                    return nil;
                }
                
                NSMutableArray *dictionaries = [NSMutableArray arrayWithCapacity:models.count];
                for (id model in models) {
                    if (model == NSNull.null) {
                        [dictionaries addObject:NSNull.null];
                        continue;
                    }
                    
                    if (![model isKindOfClass:MTLModel.class]) {
                        if (error != NULL) {
                            NSDictionary *userInfo = @{
                                                       NSLocalizedDescriptionKey: NSLocalizedString(@"Could not convert JSON array to model array", @""),
                                                       NSLocalizedFailureReasonErrorKey: [NSString stringWithFormat:NSLocalizedString(@"Expected a MTLModel or an NSNull, got: %@.", @""), model],
                                                       MTLTransformerErrorHandlingInputValueErrorKey : model
                                                       };
                            
                            *error = [NSError errorWithDomain:MTLTransformerErrorHandlingErrorDomain code:MTLTransformerErrorHandlingErrorInvalidInput userInfo:userInfo];
                        }
                        *success = NO;
                        return nil;
                    }
                    
                    NSDictionary *dict = [dictionaryTransformer reverseTransformedValue:model success:success error:error];
                    
                    if (*success == NO) return nil;
                    
                    if (dict == nil) continue;
                    
                    [dictionaries addObject:dict];
                }
                
                return dictionaries;
            }];
}

@end
