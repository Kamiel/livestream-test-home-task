//
//  FeedController.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <Foundation/Foundation.h>


@class StatusController, RACSignal<ValueType>;


@interface FeedController : NSObject

@property (nonatomic, strong, readonly, nonnull) NSArray<StatusController *> *statuses;
@property (nonatomic, strong, readonly, nonnull) RACSignal<NSError *> *error;

@end
