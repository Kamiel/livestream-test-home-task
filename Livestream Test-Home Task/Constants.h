//
//  Constants.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#import <Foundation/Foundation.h>


static NSString *const kBasePath = @"https://api.new.livestream.com/accounts/volvooceanrace/";
static NSString *const kLeg5Path = @"events/leg5"; // TODO: build api with more readable hierarchy
static NSString *const kDateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
static NSString *const kDateLocale = @"en_US";

#endif /* Constants_h */
