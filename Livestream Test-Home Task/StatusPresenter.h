//
//  StatusPresenter.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <Foundation/Foundation.h>


@class StatusController, UILabel, UICollectionViewCell;


@protocol StatusPresentation <NSObject>

@property (weak, nonatomic, readonly, nullable) UILabel *text;
@property (weak, nonatomic, readonly, nullable) UILabel *date;
@property (weak, nonatomic, readonly, nullable) UILabel *views;

@end


@interface StatusPresenter : NSObject

@property (nonatomic, strong, readwrite, nullable) StatusController *viewModeller;
@property (nonatomic, weak, readwrite, nullable) UICollectionViewCell<StatusPresentation> *view;

@end
