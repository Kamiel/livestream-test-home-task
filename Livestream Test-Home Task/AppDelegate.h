//
//  AppDelegate.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/24/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

