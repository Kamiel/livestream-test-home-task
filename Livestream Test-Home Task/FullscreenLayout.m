//
//  FullscreenLayout.m
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "FullscreenLayout.h"


@implementation FullscreenLayout

- (void)prepareLayout
{
    self.itemSize = self.collectionView.frame.size;
    self.minimumLineSpacing = 0.f;
    self.minimumInteritemSpacing = 0.f;
    self.sectionInset = UIEdgeInsetsZero;
    self.footerReferenceSize = CGSizeZero;
    self.headerReferenceSize = CGSizeZero;
}

@end
