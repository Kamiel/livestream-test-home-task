//
//  FeedController.m
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "FeedController.h"
#import "LiveStream.h"
#import "Feed.h"
#import <ReactiveObjC/ReactiveObjC.h>

#import "StatusController+Model.h"


@interface FeedController ()

@property (nonatomic, strong, readwrite, nonnull) NSArray<StatusController *> *statuses;
@property (nonatomic, strong, readwrite, nonnull) RACSignal<NSError *> *error;

@end


@implementation FeedController

- (instancetype _Nullable)init
{
    if (self = [super init]) {
        self.statuses = @[];
        [self bind];
    }
    return self;
}

- (void)bind
{
    RACSignal<NSArray<StatusController *> *> *statusesSignal =
    [[[LiveStream manager] getFeedWithProgress:nil]
     map:
     ^NSArray<StatusController *> *_Nonnull(Feed *_Nullable feed) {

         return feed.data
         ? [feed.data.rac_sequence map:
            ^StatusController *_Nonnull(Status * _Nonnull status) {

                return [[StatusController alloc]
                        initWithModel:status];
            }].array
         : @[];
     }];
    self.error = [[statusesSignal logAll] catch:
                  ^RACSignal<NSError *> *_Nonnull(NSError *_Nonnull error) {

                      return [RACSignal return:error];
                  }];
    RAC(self, statuses) = [statusesSignal catchTo:[RACSignal empty]];
}

@end
