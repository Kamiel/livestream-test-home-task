//
//  Feed.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <Mantle/Mantle.h>


@class Status;


@interface Feed : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy, readonly, nonnull) NSArray<Status *> *data;

@end
