//
//  LiveStream.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <OvercoatReactiveCocoa/OvercoatReactiveCocoa.h>


@class Feed;


NS_ASSUME_NONNULL_BEGIN

@interface LiveStream : OVCHTTPSessionManager

- (RACSignal<Feed *> *)getFeedWithProgress:(nullable id<RACSubscriber>)downloadProgress;

@end

NS_ASSUME_NONNULL_END
