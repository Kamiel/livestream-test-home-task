//
//  LiveStream.m
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "LiveStream.h"
#import "Constants.h"
#import "Feed.h"

#import <Overcoat/OVCResponse.h>
#import <ReactiveObjC/ReactiveObjC.h>


@implementation LiveStream

#pragma mark - public

- (RACSignal<Feed *> *)getFeedWithProgress:(nullable id<RACSubscriber>)downloadProgress
{
    return
    [[self rac_GET:kLeg5Path
        parameters:nil
          progress:downloadProgress]
     map:
     ^Feed *(OVCResponse *_Nullable response) {
         return (Feed *)response.result;
     }];
}

#pragma mark - routing

+ (NSDictionary<NSString *, id> *)modelClassesByResourcePath
{
    return
    @{ kLeg5Path : [Feed class] };
}

#pragma mark - override

+ (instancetype)manager
{
    return
    [[self class] new];
}

- (instancetype)init
{
    return
    [self initWithBaseURL:
     [NSURL URLWithString:kBasePath]];
}


@end
