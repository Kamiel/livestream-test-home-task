//
//  StatusCell.m
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "StatusCell.h"
#import "StatusPresenter.h"


@interface StatusCell () <StatusPresentation>

@property (weak, nonatomic, readwrite, nullable) IBOutlet UILabel *text;
@property (weak, nonatomic, readwrite, nullable) IBOutlet UILabel *date;
@property (weak, nonatomic, readwrite, nullable) IBOutlet UILabel *views;

@end


@implementation StatusCell

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder]) {
        self->_presenter = [StatusPresenter new];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.background.layer.cornerRadius = 8.f;
    self.background.layer.masksToBounds = YES;
    self.presenter.view = self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    [self.text setNeedsUpdateConstraints]; // strange bug here, forced do it manually
}

@end
