//
//  MTLJSONAdapter+ArrayConditionalTransformer.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <Mantle/Mantle.h>


NS_ASSUME_NONNULL_BEGIN

@interface MTLJSONAdapter (ArrayConditionalTransformer)

+ (NSValueTransformer<MTLTransformerErrorHandling> *)arrayTransformerWithModelClass:(Class)modelClass
                                                                        byCondition:(BOOL(^)(NSDictionary<NSString *,
                                                                                             NSString *> *_Nonnull element))condition;

@end

NS_ASSUME_NONNULL_END
