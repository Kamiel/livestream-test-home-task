//
//  StatusController+Public.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "StatusController.h"


@class RACSignal<ValueType>;


@interface StatusController (Public)

@property (nonatomic, strong, readonly, nonnull) RACSignal<NSString *> *text;
@property (nonatomic, strong, readonly, nonnull) RACSignal<NSString *> *date;
@property (nonatomic, strong, readonly, nonnull) RACSignal<NSString *> *views;

@end
