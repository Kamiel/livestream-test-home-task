//
//  FullscreenLayout.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FullscreenLayout : UICollectionViewFlowLayout

@end
