//
//  StatusPresenter.m
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "StatusPresenter.h"
#import "StatusController+Public.h"
#import <ReactiveObjC/ReactiveObjC.h>


@interface StatusPresenter ()

@end


@implementation StatusPresenter

- (instancetype _Nullable)init
{
    if (self = [super init]) {
        [self bind];
    }
    return self;
}

- (void)bind
{
    RAC(self, view.text.text) = [[RACObserve(self, viewModeller) flattenMap:
                                  ^__kindof RACSignal<NSString *> *_Nonnull
                                  (StatusController *_Nonnull viewModeller) {
                                      return viewModeller.text;
                                  }] takeUntil:
                                 self.view.rac_prepareForReuseSignal];
    RAC(self, view.date.text) = [[RACObserve(self, viewModeller) flattenMap:
                                  ^__kindof RACSignal<NSString *> *_Nonnull
                                  (StatusController *_Nonnull viewModeller) {
                                      return viewModeller.date;
                                  }] takeUntil:
                                 self.view.rac_prepareForReuseSignal];
    RAC(self, view.views.text) = [[RACObserve(self, viewModeller) flattenMap:
                                   ^__kindof RACSignal<NSString *> *_Nonnull
                                   (StatusController *_Nonnull viewModeller) {
                                       return viewModeller.views;
                                   }] takeUntil:
                                  self.view.rac_prepareForReuseSignal];
}

@end
