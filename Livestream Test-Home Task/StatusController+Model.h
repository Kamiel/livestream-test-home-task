//
//  StatusController+Model.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/27/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "StatusController.h"


@class Status;


@interface StatusController (Model)

@property (nonatomic, strong, readwrite, nonnull) Status *model;

- (instancetype _Nullable)initWithModel:(Status *_Nonnull)status;

@end
