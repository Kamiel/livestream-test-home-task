//
//  Like.h
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import <Mantle/Mantle.h>


@interface Like : MTLModel <MTLJSONSerializing>

// TODO: implement according to example below

/*
 "id": 562845,
 "created_at": "2012-04-19T02:13:40.574Z",
 "author": {
 "id": 562845,
 "full_name": "Stephen Muller",
 "short_name": null,
 "picture": null,
 "is_beta_producer": true,
 "timezone": "America/New_York",
 "description": null,
 "background_image": null,
 "background_color": null,
 "background_repeat": null,
 "background_position": null,
 "background_attachment": null,
 "created_at": "2012-04-19T02:11:10.000Z",
 "is_locked": false,
 "upcoming_events": null,
 "past_events": null,
 "links": null,
 "google_analytics_id": null,
 "mixpanel_id": "13665344334d9-0549133a1dc96b-43662242-1fa400-136653443357bf",
 "signup_page": "/",
 "signup_action": "direct",
 "devices": {
 "total": 0,
 "data": []
 },
 "category": null,
 "category_name": null,
 "private": false,
 "features": null,
 "plan_id": 1,
 "plan_info": {
 "id": 1,
 "features": "33554432",
 "type": 10,
 "is_scoped": false
 },
 "features_with_plan": "33554432",
 "is_searchable": true,
 "is_public": true,
 "ad_account_id": null,
 "ad_provider_id": null,
 "ad_custom_params": {
 "accountId": "%accountId%",
 "eventId": "%eventId%",
 "live": "%isLive%",
 "embedPath": "%embedPath%",
 "embedHost": "%embedHost%"
 },
 "ad_enabled_for_vod": false,
 "ad_enabled_for_live": false,
 "ad_enabled_for_owner": true,
 "ad_types": [
 "preroll"
 ],
 "ad_enabled": false,
 "followers": null,
 "following": null
 }
*/

@end
