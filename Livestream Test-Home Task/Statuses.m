//
//  Statuses.m
//  Livestream Test-Home Task
//
//  Created by Yuriy Pitomets on 6/26/17.
//  Copyright © 2017 Yuriy Pitomets. All rights reserved.
//

#import "Statuses.h"
#import "StatusCell.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>

#import "FeedController.h"
#import <ReactiveObjC/ReactiveObjC.h>
#import "StatusPresenter.h"


@interface Statuses () <UICollectionViewDelegateFlowLayout, UICollectionViewDataSourcePrefetching, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (strong, nonatomic, readonly, nonnull) FeedController *viewModeller;

@end


@implementation Statuses

- (void)viewDidLoad
{
    [super viewDidLoad];

    if ([self.collectionView respondsToSelector:
         @selector(setPrefetchDataSource:)]) {
        self.collectionView.prefetchDataSource = self;
    }
    // empty data case
    self.collectionView.emptyDataSetSource = self;
    self.collectionView.emptyDataSetDelegate = self;

    // TODO: move viewModeller, DataSource and data logic to StatusesPresenter.
    self->_viewModeller = [FeedController new];

    @weakify(self);
    [[RACObserve(self, viewModeller.statuses) skip:1] subscribeNext:
     ^(id  _Nullable __unused _) {
         @strongify(self);
         [self.activity stopAnimating];
         [self.collectionView reloadData];
     }];
    [self.viewModeller.error subscribeNext:
     ^(NSError *_Nullable error) {
         [self.activity stopAnimating];
     }];

    // activity
    [self.view addSubview:self.activity];
    self.activity.center = CGPointMake(roundf(0.5f * self.view.bounds.size.width),
                                       roundf(0.5f * self.view.bounds.size.height));
    [self.view bringSubviewToFront:self.activity];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    // TODO: cleanup models
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark <DZNEmptyDataSetSource>

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    return
    [[NSAttributedString alloc] initWithString:
     NSLocalizedString(@"No statuses to see yet",
                       @"No statuses available title.")
                                    attributes:nil];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    return
    [[NSAttributedString alloc] initWithString:
     NSLocalizedString(@"Try to check later",
                       @"No statuses available description.")
                                    attributes:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSourcePrefetching>

- (void)     collectionView:(UICollectionView *)collectionView
  prefetchItemsAtIndexPaths:(NSArray<NSIndexPath *> *)indexPaths
{
    // just nothing for now
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.viewModeller.statuses.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    StatusCell *cell =
    [collectionView dequeueReusableCellWithReuseIdentifier:
     NSStringFromClass([StatusCell class])
                                              forIndexPath:indexPath];
    
    // Configure the cell
    cell.presenter.viewModeller = self.viewModeller.statuses[indexPath.row];

    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

#pragma mark <UICollectionViewDelegateFlowLayout>

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.view.frame.size;
}

#pragma mark - UIContentContainer

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id <UIViewControllerTransitionCoordinator>)coordinator
{
    [super viewWillTransitionToSize:size
          withTransitionCoordinator:coordinator];

    @weakify(self);
    [coordinator animateAlongsideTransition:
     ^(id<UIViewControllerTransitionCoordinatorContext> context) {
         @strongify(self);
         // TODO: improve animation
         [self.collectionView.collectionViewLayout invalidateLayout];

         if (self.collectionView.visibleCells &&
             self.collectionView.visibleCells.count > 0) {
             [self.collectionView scrollToItemAtIndexPath:[self.collectionView indexPathForCell:self.collectionView.visibleCells[0]]
                                         atScrollPosition:UICollectionViewScrollPositionNone
                                                 animated:NO];
         }

     } completion:nil];
}

@end
